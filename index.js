//import thư viện express JS
const express = require ("express");

//Khai báo app 
const app = express ();

//khai báo cổng chạy app
const port = 8000;
//import thư viện path
const path = require ("path");

//khai báo router
const randomNumberRouter = require("./app/routes/randomNumberRouter");

//app sử dụng middleware mỗi lần chạy
app.use("/api", randomNumberRouter);

//call api chạy browser
app.get("/",(request, response)=>{
    response.sendFile(path.join(__dirname + "/views/Task 31.30.html"))
});
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));
//app chạy trên cổng đã khai báo
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`);
})